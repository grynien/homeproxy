
var http = require('http'),
    simplerouter = require('./simplerouter'),
    homeip = require('./homeip');
    
var ipaddress = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.OPENSHIFT_NODEJS_PORT  || 8080;


 

var server = http.createServer(function(req, res) {
  simplerouter.handleRequest(req,res);  
});

server.on('upgrade', function (req, socket, head) {
  simplerouter.handleWebSocket(req, socket, head); 
});


server.listen(port, ipaddress);