var httpProxy = require('http-proxy'),
    homeip = require('./homeip');
   
    
var proxy = httpProxy.createProxyServer({ws: true});

proxy.on('error', function (err, req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Server is down');
});
 

function handleRequest(req, res){
    var method = req.method.toLowerCase();
    var url = require('url').parse(req.url, true);
    
    if(method == 'post' && url.pathname == '/updatetarget') {
		var body = '';
		req.on('data', function(data) {
			body += data;
		});
		req.on('end', function() {
			var post = require('querystring').parse(body);
			req.body = body;
			req.params = post;
			handleUpgrade(req, res);
		});
	} else{
        proxy.web(req, res, { target: 'https://' + homeip.getTarget() });
    }
}


function handleWebSocket(req, socket, head){
      proxy.ws(req, socket, head, { target: homeip.getTarget() });
}



function handleUpgrade(req, res) {
		
        var ip = req.body;
        homeip.saveIp(ip);
        
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Got Post Data');

}



exports.handleRequest = handleRequest;

exports.handleWebSocket = handleWebSocket;